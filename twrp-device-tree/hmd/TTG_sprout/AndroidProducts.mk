#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TTG_sprout.mk

COMMON_LUNCH_CHOICES := \
    omni_TTG_sprout-user \
    omni_TTG_sprout-userdebug \
    omni_TTG_sprout-eng
